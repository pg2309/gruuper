package org.giancola.gruuper;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class MyActivity extends Activity implements OnClickListener
{
    public static String LOGTAG = "Peep";
    public static Gruuper ms_gruuper = new Gruuper();


    private Button allToMe;
    private Button imHere;
    private Button whereIs;
    private TextView activeMsg;
    private TextView mainList;
    private static String phoneNum = "4438515500";
    private SMSReceiver m_smsReceiver = new SMSReceiver();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.d(LOGTAG, "onCreate");
        setContentView(R.layout.activity_my);
        ms_gruuper.setM_activity(this);
        m_smsReceiver.setM_gruuper(ms_gruuper);

        allToMe = (Button)findViewById(R.id.allToMeButton);
        imHere = (Button)findViewById(R.id.imHereButton);
        whereIs = (Button)findViewById(R.id.whereIsButton);
        activeMsg = (TextView)findViewById(R.id.textView);
        mainList = (TextView)findViewById(R.id.textView2);

        allToMe.setOnClickListener(this);
        imHere.setOnClickListener(this);
        whereIs.setOnClickListener(this);

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.d(LOGTAG, "onStart");
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Log.d(LOGTAG, "onPause");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.d(LOGTAG, "onResume");
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
        Log.d(LOGTAG, "onRestart");
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        Log.d(LOGTAG, "onStop");
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.d(LOGTAG, "onDestroy");
    }

    @Override
    public void onClick(View view)
    {
        Log.d(LOGTAG, "onClick");
        if (allToMe == view)
        {
            ms_gruuper.AllToMe();
            Log.d(LOGTAG, "All To Me");
        }
        else if (imHere == view)
        {
            ms_gruuper.ImHere();
            Log.d(LOGTAG, "I'm Here");
        }
        else if (whereIs == view)
        {
            ms_gruuper.WhereIs();
            Log.d(LOGTAG, "Where Is?");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public TextView getMainList()
    {
        return mainList;
    }

}
