package org.giancola.gruuper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by pagianc on 10/12/14.
 */
public class SMSReceiver extends BroadcastReceiver
{
    public SMSReceiver()
    {
        Log.d(MyActivity.LOGTAG, "This is the SMSReceiver constructor " + this);
    }

    public void setM_gruuper(Gruuper gruuper)
    {
        Log.d(MyActivity.LOGTAG, "setting m_gruuper");
        m_gruuper = gruuper;
        if (null != m_gruuper)
        {
            Log.d(MyActivity.LOGTAG, "m_gruuper set to good value");
        }
        else
        {
            Log.d(MyActivity.LOGTAG, "Looks like m_gruuper was set to null!");
        }
    }

    private Gruuper m_gruuper = null;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d(MyActivity.LOGTAG, "Enter SMSReceiver::onReceive() " + this);

        // Read the SMS message.
        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;
        if (bundle != null)
        {
            //---retrieve the SMS message received---
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];
            for (int i = 0; i < msgs.length; ++i)
            {
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                String phoneNumber = msgs[i].getOriginatingAddress();
                String message = msgs[i].getMessageBody();

                Log.d(MyActivity.LOGTAG, "SMS from " + phoneNumber + ": " + message + "\n");

                if (null != MyActivity.ms_gruuper)
                {
                    MyActivity.ms_gruuper.receivedSMS(phoneNumber, message);
                }
                else
                {
                    Log.d(MyActivity.LOGTAG, "Looks like m_gruuper was null!");
                }
            }
        }
    }
}
