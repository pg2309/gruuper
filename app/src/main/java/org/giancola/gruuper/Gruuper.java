package org.giancola.gruuper;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.telephony.SmsManager;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.giancola.statemachine.*;

/**
 * Created by pagianc on 10/12/14.
 */
public class Gruuper
{
    public static String ms_theMarker = "@GRUUPER@";
    public static String ms_thePattern = ms_theMarker + "(.*)";
    public static String ms_imhere = "ImHere";
    public static String ms_alltome = "AllToMe";
    public static String ms_whereis = "WhereIs";

    private ArrayList<GroupEntry> m_groups = new ArrayList<GroupEntry>();
    private MyActivity m_activity = null;
    private GroupEntry m_currGroup = null;

    public Gruuper()
    {
        GroupEntry entry = new GroupEntry();
//        entry.add(new GroupMember("Tony", "5052396327"));
//        entry.add(new GroupMember("Ellen", "4438515500"));
//        entry.add(new GroupMember("Katy", "4104403109"));
        entry.add(new GroupMember("Peep", "4438018958"));
        setM_currGroup(entry);
    }

    public void SmsReceive(String phoneNumber, String msg)
    {
        Log.d(MyActivity.LOGTAG, "Received SMS message:");
        Log.d(MyActivity.LOGTAG, msg);
    }

    public void setM_activity(MyActivity m_activity)
    {
        this.m_activity = m_activity;
    }

    public GroupEntry getM_currGroup()
    {
        return m_currGroup;
    }

    public void setM_currGroup(GroupEntry m_currGroup)
    {
        this.m_currGroup = m_currGroup;
    }

    public Location getPos()
    {
        Location from;
        {
            LocationManager lm = (LocationManager)m_activity.getSystemService(Context.LOCATION_SERVICE);
            Criteria crit = new Criteria();
            crit.setAccuracy(Criteria.ACCURACY_COARSE);
            String provider = lm.getBestProvider(crit, true);
            from = lm.getLastKnownLocation(provider);
        }

        // Make sure we have a location.
        if (null == from)
        {
            Log.d(MyActivity.LOGTAG, "Did not get location.");
        }
        else
        {
            Log.d(MyActivity.LOGTAG, "Pos: " + from.toString());
        }
        return from;
    }

    public void AllToMe()
    {
        Location from = getPos();
        String pos = ms_alltome + ":pos:" + from.getLatitude() + "," + from.getLongitude() + "," + from.getAltitude();
        sendToGroup(pos, m_currGroup);
    }

    public void ImHere()
    {
        Location from = getPos();
        String pos = ms_imhere + ":pos:" + from.getLatitude() + "," + from.getLongitude() + "," + from.getAltitude();
        sendToGroup(pos, m_currGroup);
    }

    public void WhereIs()
    {
    }

    public void sendToMember(String message, String name)
    {
        for (GroupMember member : m_currGroup)
        {
            if (member.getM_userName() == name)
            {
                sendSMS(member.getM_phoneNum(), message);
            }
        }
    }

    public void sendToMember(String message, GroupMember member)
    {
        sendSMS(member.getM_phoneNum(), message);
    }

    public void sendToGroup(String message, GroupEntry group)
    {
        for (GroupMember member : group)
        {
            sendSMS(member.getM_phoneNum(), message);
        }
    }

    public void receivedSMS(String phoneNumber, String message)
    {
        String theMessage = "";

        Log.d(MyActivity.LOGTAG, "Received an SMS");
        Log.d(MyActivity.LOGTAG, "phone: " + phoneNumber);
        Log.d(MyActivity.LOGTAG, "message: <" + message + ">");
        Log.d(MyActivity.LOGTAG, "pattern: <" + ms_thePattern + ">");

        // Check for a Gruuper message.
        Pattern pattern = Pattern.compile(ms_thePattern);
        Matcher matcher = pattern.matcher(message);
        if (matcher.find())
        {
            Log.d(MyActivity.LOGTAG, "Found the GRUUPER header");
            theMessage = matcher.group(1);
            Log.d(MyActivity.LOGTAG, "The message: <" + theMessage + ">");
            parseMessage(phoneNumber, theMessage);
        }
        else
        {
            Log.d(MyActivity.LOGTAG, "Did not find the GRUUPER header");
        }
    }

    public Location extractPos(String msg)
    {
        String lat;
        String lon;
        String alt;
        Location loc = null;

        Pattern pattern = Pattern.compile("^.*:pos:([0-9.-]*),([0-9.-]*),([0-9.-]*).*$");
        Matcher matcher = pattern.matcher(msg);

        if (matcher.find())
        {
            lat = matcher.group(1);
            lon = matcher.group(2);
            alt = matcher.group(3);

            double latd = Double.parseDouble(lat);
            double lond = Double.parseDouble(lon);
            double altd = Double.parseDouble(alt);

lond += 0.005;

            Log.d(MyActivity.LOGTAG, "Got a pos: lat " + lat + ", lon " + lon + ", alt " + alt);

            String rpt = String.format("from doubles: lat %10.6f, lon %10.6f, alt %10.3f", latd, lond, altd);
            Log.d(MyActivity.LOGTAG, rpt);

            loc = new Location("self");
            loc.setLatitude(latd);
            loc.setLongitude(lond);
            loc.setAltitude(altd);
        }
        return loc;
    }

    public void parseMessage(String phnum, String msg)
    {
        Log.d(MyActivity.LOGTAG, "Who sent message?");
        for (GroupMember member : m_currGroup)
        {
            Log.d(MyActivity.LOGTAG, "Is it " + member.getM_userName());
            String patt = member.getM_phoneNum();
            Pattern pattern = Pattern.compile(member.getM_phoneNum());
            Matcher matcher = pattern.matcher(phnum);
            if (matcher.find())
            {
                Log.d(MyActivity.LOGTAG, "Message to: " + member.getM_userName());
                Log.d(MyActivity.LOGTAG, "Pos string: " + msg);
                if (Pattern.compile(ms_alltome + ":pos:(.*)").matcher(msg).find())
                {
                    Log.d(MyActivity.LOGTAG, "It's 'all to me'");
                }
                else if (Pattern.compile(ms_imhere + ":pos:(.*)").matcher(msg).find())
                {
                    Log.d(MyActivity.LOGTAG, "It's 'im here'");
                    pattern = Pattern.compile(ms_imhere + ":pos:(.*)");
                    matcher = pattern.matcher(msg);
                    Location pos = extractPos(msg);
                    Log.d(MyActivity.LOGTAG, "Got pos: " + pos);
                    member.setM_lastLoc(pos);
                    reportGroupLocs();
                }
                else if (Pattern.compile(ms_whereis + ":pos:(.*)").matcher(msg).find())
                {
                    Log.d(MyActivity.LOGTAG, "It's 'where is'");
                }
                else
                {
                    Log.d(MyActivity.LOGTAG, "Didn't recognize");
                }
            }
            else
            {
                Log.d(MyActivity.LOGTAG, "No!");
            }
        }
    }

    private void reportGroupLocs()
    {
        String report = "";
        Location here = getPos();
        for (GroupMember member : m_currGroup)
        {
            Location loc = member.getM_lastLoc();
            double dist = here.distanceTo(loc);
            double bear = here.bearingTo(loc);
            String line = String.format("%s: %5.1fft, dir: %5.2f\n", member.getM_userName(), dist, bear);
            Log.d(MyActivity.LOGTAG, "Reporting: " + line);
            report += line;
        }
        m_activity.getMainList().setText(report);
    }



    //---sends an SMS message to another device---
    private void sendSMS(String phoneNumber, String message)
    {
        Log.d(MyActivity.LOGTAG, "Sending SMS message");
        Log.d(MyActivity.LOGTAG, phoneNumber);
        Log.d(MyActivity.LOGTAG, message);
        String fullMessage = ms_theMarker + message;
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, fullMessage, null, null);
    }


















/*
        Location from = new Location("self");
        if (m_from == -1)
        {
            LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            Criteria crit = new Criteria();
            crit.setAccuracy(Criteria.ACCURACY_COARSE);
            String provider = lm.getBestProvider(crit, true);
            from = lm.getLastKnownLocation(provider);
        }
        else
        {
            from.setLatitude(m_fromLat[m_from]);
            from.setLongitude(m_fromLon[m_from]);
        }

        // Make sure we have a location.
        if (null == from)
        {
            mReportText.append("No current location available.");
            return;
        }

        // Now find the stop that's closest to us.
        int closestStop = -1;
        float leastDist = (float)99999999.0;
        for (int i = 0; i < 3; i++)
        {
            Location dest = new Location("self");
            dest.setLatitude(m_stopLat[i]);
            dest.setLongitude(m_stopLon[i]);

            // Calculate the distance to this stop.
            float dist = from.distanceTo(dest);

            // See if this is the closest one we've seen.
            if (dist < leastDist)
            {
                leastDist = dist;
                closestStop = i;
            }
            mReportText.append(String.format("%15s: %10.0f\n", m_stopNames[i],
                        dist));
        }
 */
}

