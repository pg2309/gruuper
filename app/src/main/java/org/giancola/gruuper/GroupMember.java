package org.giancola.gruuper;

import android.location.Location;

/**
 * Created by pagianc on 10/12/14.
 */
public class GroupMember
{
    private String m_userName;
    private String m_phoneNum;
    private int[] m_ipAddr;
    private Location m_lastLoc;

    public Location getM_lastLoc()
    {
        return m_lastLoc;
    }

    public void setM_lastLoc(Location m_lastLoc)
    {
        this.m_lastLoc = m_lastLoc;
    }

    public GroupMember(String m_userName, String m_phoneNum) {
        this.m_userName = m_userName;
        this.m_phoneNum = m_phoneNum;
    }

    public String getM_userName() {
        return m_userName;
    }

    public void setM_userName(String m_userName) {
        this.m_userName = m_userName;
    }

    public String getM_phoneNum() {
        return m_phoneNum;
    }

    public void setM_phoneNum(String m_phoneNum) {
        this.m_phoneNum = m_phoneNum;
    }

    public int[] getM_ipAddr() {
        return m_ipAddr;
    }

    public void setM_ipAddr(int[] m_ipAddr) {
        this.m_ipAddr = m_ipAddr;
    }
}
